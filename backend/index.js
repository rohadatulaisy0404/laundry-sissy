const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const PORT = process.env.PORT || 8000;
const mainPath = "/api/";
const app = express();
app.use(bodyParser.json());
app.use(
    cors({
        origin: "*",
    })
);

const userRoute = require("./src/routes/UserRoute");
const layananRoute = require("./src/routes/layananRoute");
const pesanRoute = require("./src/routes/pesanRoute");
const antarRoute = require("./src/routes/antarRoute");
const bayarRoute = require("./src/routes/bayarRoute");

app.use(mainPath, userRoute);
app.use(mainPath, layananRoute);
app.use(mainPath, pesanRoute);
app.use(mainPath, antarRoute);
app.use(mainPath, bayarRoute);


app.listen(PORT, () => {
        console.log("localhost up and running...", PORT);
    });