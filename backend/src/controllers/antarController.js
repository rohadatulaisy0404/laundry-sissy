const {
    getAllAntar,
    insertAntar,
    deleteAntar,
    updateAntar

} = require("../models/antarModel");

exports.getAntar = (req, res) => {
    const querySql = "SELECT * FROM pengantaran";
    getAllAntar(res, querySql);
};

exports.getAntarById = (req, res) => {
    const id = req.params.id_pengantaran;
    const querySql = `SELECT * FROM pengantaran WHERE id_pengantaran = ${id}`;
    getAllAntar(res, querySql);
};
exports.updateAntar = (req, res) => {
    const data = { ...req.body };
    const querySearch = "SELECT * FROM pengantaran WHERE id_pengantaran = ?";
    const queryUpdate = "UPDATE pengantaran SET ? WHERE id_pengantaran = ?";
    
    updateAntar(res, querySearch, queryUpdate, req.params.id_pengantaran, data);
};

exports.createAntar = (req, res) => {
    const data = { ...req.body };
    const querySql = "INSERT INTO pengantaran SET ?";
    insertAntar(res, querySql, data);
};

exports.deleteAntar = (req, res) => {
    const querySearch = "SELECT * FROM pengantaran WHERE id_pengantaran = ?";
    const queryDelete = "DELETE FROM pengantaran WHERE id_pengantaran = ?";
    deleteLayanan(res, querySearch, queryDelete, parseInt(req.params.id_pengantaran));
};

