const {
    getAllBayar,
    insertBayar,
    deleteBayar,
    updateBayar

} = require("../models/bayarModel");

exports.getBayar = (req, res) => {
    const querySql = "SELECT * FROM pembayaran";
    getAllBayar(res, querySql);
};

exports.getBayarById = (req, res) => {
    const id = req.params.id_pembayaran;
    const querySql = `SELECT * FROM pembayaran WHERE id_pembayaran = ${id}`;
    getAllBayar(res, querySql);
};
exports.updateBayar = (req, res) => {
    const data = { ...req.body };
    const querySearch = "SELECT * FROM pembayaran WHERE id_pembayaran = ?";
    const queryUpdate = "UPDATE pembayran SET ? WHERE id_pembayaran = ?";
    
    updateBayar(res, querySearch, queryUpdate, req.params.id_pembayaran, data);
};

exports.createBayar = (req, res) => {
    const data = { ...req.body };
    const querySql = "INSERT INTO pembayaran SET ?";
    insertBayar(res, querySql, data);
};

exports.deleteBayar = (req, res) => {
    const querySearch = "SELECT * FROM pembayaran WHERE id_pembayaran = ?";
    const queryDelete = "DELETE FROM pembayaran WHERE id_pembayaran = ?";
    deleteBayar(res, querySearch, queryDelete, parseInt(req.params.id_pembayaran));
};

