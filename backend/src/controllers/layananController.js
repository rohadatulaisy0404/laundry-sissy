const {
    getAllLayanan,
    insertLayanan,
    deleteLayanan,
    updateLayanan

} = require("../models/layananModel");

exports.getLayanan = (req, res) => {
    const querySql = "SELECT * FROM layanan";
    getAllLayanan(res, querySql);
};

exports.getLayananById = (req, res) => {
    const id = req.params.id;
    const querySql = `SELECT * FROM layanan WHERE id = ${id}`;
    getAllLayanan(res, querySql);
};
exports.updateLayanan = (req, res) => {
    const data = { ...req.body };
    const querySearch = "SELECT * FROM layanan WHERE id = ?";
    const queryUpdate = "UPDATE layanan SET ? WHERE id = ?";
    
    updateLayanan(res, querySearch, queryUpdate, req.params.id, data);
};

exports.createLayanan = (req, res) => {
    const data = { ...req.body };
    const querySql = "INSERT INTO layanan SET ?";
    insertLayanan(res, querySql, data);
};

exports.deleteLayanan = (req, res) => {
    const querySearch = "SELECT * FROM layanan WHERE id = ?";
    const queryDelete = "DELETE FROM layanan WHERE id = ?";
    deleteLayanan(res, querySearch, queryDelete, parseInt(req.params.id));
};

