const {
    getAllPesan,
    insertPesan,
    deletePesan
    // updatePesan

} = require("../models/pesanModel");

exports.getPesan = (req, res) => {
    const querySql = "SELECT * FROM pemesanan";
    getAllPesan(res, querySql);
};

exports.getPesanById = (req, res) => {
    const id = req.params.id_pemesanan;
    const querySql = `SELECT * FROM pemesanan WHERE id_pemesanan = ${id}`;
    getAllPesan(res, querySql);
};

// exports.updatePesan = (req, res) => {
//     const data = { ...req.body };
//     const querySearch = "SELECT * FROM layanan WHERE id = ?";
//     const queryUpdate = "UPDATE layanan SET ? WHERE id = ?";
    
//     updateLayanan(res, querySearch, queryUpdate, req.params.id, data);
// };

exports.createPesan = (req, res) => {
    const data = { ...req.body };
    const querySql = "INSERT INTO pemesanan SET ?";
    insertPesan(res, querySql, data);
};

exports.deletePesan = (req, res) => {
    const querySearch = "SELECT * FROM pemesanan WHERE id_pemesanan = ?";
    const queryDelete = "DELETE FROM pemesanan WHERE id_pemesanan = ?";
    deletePesan(res, querySearch, queryDelete, parseInt(req.params.id_pemesanan));
};

