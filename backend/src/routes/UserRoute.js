const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController");

// mengambil data user
router.get("/user", UserController.getAllUser);
router.get("/user/:email&:password", UserController.getUser);

// merubah sebagian data user
router.put("/user/:id", UserController.updateUser);

// menambah data user
router.post("/user/", UserController.createUser);

// menghapus user
router.delete("/user/:id", UserController.deleteUser);

module.exports = router;
