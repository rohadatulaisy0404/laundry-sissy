const express = require("express");
const router = express.Router();
const antarController = require("../controllers/antarController");

// mengambil data antar
router.get("/pengantaran", antarController.getAntar);
router.get("/pengantaran/:id_pengantaran", antarController.getAntarById);

// merubah sebagian data antar
router.put("/pengantaran/:id_pengantaran", antarController.updateAntar);
// menambah data antar
router.post("/pengantaran", antarController.createAntar);

// menghapus antar
router.delete("/pengantaran/:id_pengantaran", antarController.deleteAntar);

module.exports = router;
