const express = require("express");
const router = express.Router();
const bayarController = require("../controllers/bayarController");

// mengambil data antar
router.get("/pembayaran", bayarController.getBayar);
router.get("/pembayaran/:id_pembayaran", bayarController.getBayarById);

// merubah sebagian data bayar
router.put("/pembayaran/:id_pembayaran", bayarController.updateBayar);
// menambah data bayar
router.post("/pembayaran", bayarController.createBayar);

// menghapus bayar
router.delete("/pembayaran/:id_pembayaran", bayarController.deleteBayar);

module.exports = router;
