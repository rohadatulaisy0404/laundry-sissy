const express = require("express");
const router = express.Router();
const layananController = require("../controllers/layananController");

// mengambil data user
router.get("/layanan", layananController.getLayanan);
router.get("/layanan/:id", layananController.getLayananById);

// merubah sebagian data user
router.put("/layanan/:id", layananController.updateLayanan);
// menambah data user
router.post("/layanan", layananController.createLayanan);

// menghapus user
router.delete("/layanan/:id", layananController.deleteLayanan);

module.exports = router;
