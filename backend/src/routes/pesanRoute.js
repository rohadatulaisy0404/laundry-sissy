const express = require("express");
const router = express.Router();
const pesanController = require("../controllers/pesanController");

// mengambil data user
router.get("/pemesanan", pesanController.getPesan);
router.get("/pemesanan/:id_pemesanan", pesanController.getPesanById);

// merubah sebagian data user
// router.put("/layanan/:id", pesanController.updatePesan);
// menambah data user
router.post("/pemesanan", pesanController.createPesan);

// menghapus user
router.delete("/pemesanan/:id_pemesanan", pesanController.deletePesan);

module.exports = router;
